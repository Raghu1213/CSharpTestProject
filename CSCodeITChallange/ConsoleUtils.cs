﻿using System;
namespace CSCodeITChallange
{
    public  static class ConsoleUtils
    {
        [System.Diagnostics.Conditional("DEBUG")]
        public static void Write (object str =null)
        {
            Console.Write(str);
        }
		[System.Diagnostics.Conditional("DEBUG")]
        public static void WriteLine(object  str =null)
		{
			Console.WriteLine(str);
		}
    }
}
