﻿using System;
using System.Collections;

namespace CSCodeITChallange
{
    public struct Coordinate
    {
        public Coordinate( int x , int y)
        {
            this.X = x;
            this.Y = y;
            this.IsBorder = false;
        }
        public int X { get; internal set; }
        public int Y { get; internal set; }
        public bool IsBorder { get; set; }

        public override string ToString()
        {
            return string.Format("({0},{1})", X, Y);
        }

        public override bool Equals(object obj)
        {
            var toCompare = (Coordinate)obj;
            return this.X == toCompare.X && this.Y == toCompare.Y;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}