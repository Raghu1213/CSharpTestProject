﻿using System;
using System.Collections.Generic;
using CSCodeITChallange.Shapes;

namespace CSCodeITChallange
{
    public interface IGeomericShape
    {
        /// <summary>
        /// Gets All edges of the shape
        /// </summary>
        /// <value>The edges.</value>
        IList<Coordinate> Edges { get; set; }

        /// <summary>
        /// Gets all coordinates in a shape
        /// </summary>
        /// <returns>The all coordinates.</returns>
		IList<Coordinate> GetAllCoordinates();

        /// <summary>
        /// Gets the border coordinates.
        /// </summary>
        /// <returns>The border coordinates.</returns>
		IList<Coordinate> GetBorderCoordinates();

		/// <summary>
		/// Finds the intersection. i.e. all points intersecting with the container
		/// </summary>
		/// <returns>The intersection.</returns>
		/// <param name="childShape">Container shape.</param>
		IntersectionPoints FindIntersection(IGeomericShape childShape);

        /// <summary>
        /// Gets sub shape of the child shape that is inside the container.
        /// </summary>
        /// <returns>The child dimentiones.</returns>
        /// <param name="intersections">Intersections.</param>
        ChildShapeProperties ExtractSubObject(IntersectionPoints intersections);

        /// <summary>
        /// Calculates area of the object
        /// </summary>
        /// <returns>The area.</returns>
        double GetArea();

        /// <summary>
        /// Draw the object.
        /// </summary>
        void Draw();

        /// <summary>
        /// Prints all coordinate information
        /// </summary>
        void Print();


    }
}
