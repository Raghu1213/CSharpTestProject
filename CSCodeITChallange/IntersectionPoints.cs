﻿using System.Collections.Generic;
using System.Linq;

namespace CSCodeITChallange
{
    public class IntersectionPoints
    {
        private IEnumerable<Coordinate> intersections;
        private IEnumerable<Coordinate> edgesFallingInside;

        public IntersectionPoints(IEnumerable<Coordinate> intersections, IEnumerable<Coordinate> edgesFallingInside)
        {
            this.intersections = intersections;
            this.edgesFallingInside = edgesFallingInside;
        }

        public IEnumerable<Coordinate> AllIntersections
        {
            get { return intersections; }
        }
        public IEnumerable<Coordinate> EdgesFallingInside
        {
            get { return edgesFallingInside; }
        }

        public void Print()
        {
            ConsoleUtils.WriteLine("All intersecting points-->");
            ConsoleUtils.WriteLine(string.Join(",", AllIntersections));

            ConsoleUtils.WriteLine("All edges falling inside-->");
            ConsoleUtils.WriteLine(string.Join(",", edgesFallingInside));
        }

    }
}