﻿using System;
using System.Drawing;
using CSCodeITChallange.Shapes;


namespace CSCodeITChallange
{
    class MainClass
    {
        static Func<string, int> getInput = (msg) =>
            {
                Console.Write(msg + ":");
                return Console.ReadLine().ToInt();
            };

        public static void Main(string[] args)
        {
            args = new string[] { "Circle" };
            switch (args[0])
            {
                case "Rectangle":
                    DrawRectangle();
                    break;
                case "Circle":
                    DrawCircle();
                    break;
                default:
                    break;
            }
        }

        private static void DrawCircle()
        {
			Console.Clear();
			Console.WriteLine("Enter Parent Rectangle:-->*************************");
			IGeomericShape parentRec = new GRectangle(new Coordinate(getInput("x"), getInput("y")), getInput("Width"), getInput("Height"));
			parentRec.Print();
            Console.WriteLine("Area burte force-->" + parentRec.GetAllCoordinates().Count);
            Console.WriteLine("Area-->" + parentRec.GetArea());
            Console.Clear();
			
			Console.WriteLine("Enter Child:-->*************************");
            IGeomericShape childCircle = new Circle(new Coordinate(getInput("x0"), getInput("y0")), getInput("Radius"));
            Console.WriteLine("Area of Circle Brute Force --> " + childCircle.GetAllCoordinates().Count);
            Console.WriteLine("Area of Circle --> " + childCircle.GetArea());
            childCircle.Print();

            var intersection =  parentRec.FindIntersection(childCircle);
            intersection.Print();

            var subShape = childCircle.ExtractSubObject(intersection);
            subShape.Print();

			var result = parentRec.GetArea() - subShape.GetArea();

			Console.WriteLine("Empty Area  --> " + result);

           // ShapeOperations.CombineAndPrintTwoShapes(parentRec, subShape);

			// childCircle.Draw();

		}

        private static void DrawRectangle()
        {
            

            Console.WriteLine("Enter Parent:-->*************************");
            IGeomericShape parentRec = new GRectangle(new Coordinate(getInput("x"), getInput("y")), getInput("Width"), getInput("Height"));
            parentRec.Print();

            Console.WriteLine("Enter Child:-->*************************");
            IGeomericShape childRec = new GRectangle(new Coordinate(getInput("x"), getInput("y")), getInput("Width"), getInput("Height"));
            childRec.Print();

            var intersectingCoordinates = parentRec.FindIntersection(childRec);
            intersectingCoordinates.Print();

            var subShape = childRec.ExtractSubObject(intersectingCoordinates);
            subShape.Print();

            var result = parentRec.GetArea() - subShape.GetArea();

            Console.WriteLine("Empty Area  --> " + result);
        }



    }
}