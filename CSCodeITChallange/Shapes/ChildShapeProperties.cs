﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSCodeITChallange.Shapes
{
    /// <summary>
    /// Holds sub object properties
    /// </summary>
    public class ChildShapeProperties
    {
        public ChildShapeProperties(IList<Coordinate> edges, IDictionary<string, int> variables)
        {
            this.Edges = edges?? new List<Coordinate>();
            this.Variables = variables?? new Dictionary<string,int>();
        }

        public IList<Coordinate> Edges { get; set; }

        public IDictionary<string, int> Variables { get; set; }

        public override string ToString()
        {
            string param = string.Empty;
            foreach (var variable in Variables)
            {
                param  = param + "[" + variable.Key + ":" + variable.Value + "]" ;
            }
            return string.Format("Sub Shape Properteis --> \r\nEdges-->{0}\r\nVariables-->{1}",string.Join(",", Edges), param);
        }

        public void Print ()
        {
            Console.WriteLine(this);
        }

        public double GetArea ()
        {
            int area = 0;
            //rectangle will have width and height
            if (Variables.Count() ==2 && Variables.ContainsKey("WIDTH") && Variables.ContainsKey("HEIGHT"))
            {
                area = Variables["WIDTH"] * Variables["HEIGHT"];
            }
            else if (Variables.Count()==1 && Variables.ContainsKey("CIRCLE"))
            {
                //var grouped = this.Edges.GroupBy(e => e.Y, (y, row) => new { Y = y, Row = row.OrderBy(r => r.X) }) ;
                return this.Edges.Count();

            }
            return area;
        }

    }
}

