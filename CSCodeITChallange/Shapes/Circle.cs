﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSCodeITChallange.Shapes
{
    public class Circle : IGeomericShape
    {
        #region Private Properties
        private Coordinate Center { get; set; }
        private int Radius { get; set; }
        private IList<Coordinate> AllCoordinates { get; set; } = new List<Coordinate>();
        private int MaxX
        {
            get
            {
                return this.Center.X + Radius;
            }
        }
        private int MinX
        {
            get
            {
                return this.Center.X - Radius;
            }
        }
        private int MaxY
        {
            get
            {
                return this.Center.Y + Radius;
            }
        }
        private int MinY
        {
            get
            {
                return this.Center.Y - Radius;

            }
        }
        public Circle(Coordinate center, int radius)
        {
            Radius = radius;
            Center = center;
            this.Edges = GetBorderCoordinates();
        }
        #endregion

        #region IGeomericShape
        public IList<Coordinate> Edges 
        {
            get;set;

        }

       
        public void Draw()
        {
            var result = string.Empty;
            var allCoordinates = this.GetAllCoordinates();
            var grouped = allCoordinates.GroupBy(p => p.Y, (Y, points) => new { Y = Y, Points = points.OrderBy(p => p.X) });
            foreach (var row in grouped)
            {
                result = string.Concat(result, new string(' ', row.Points.First().X), new string('#', row.Points.Count()), Environment.NewLine);
            }
            Console.WriteLine(result);
        }

        public ChildShapeProperties ExtractSubObject(IntersectionPoints intersections)
        {   
            var pointsInsideParent = intersections.AllIntersections.Union(intersections.EdgesFallingInside);
            return new ChildShapeProperties(pointsInsideParent.ToList(), new Dictionary<string,int>{{"CIRCLE",this.Radius}});
        }

        public IntersectionPoints FindIntersection(IGeomericShape childShape)
        {
            throw new Exception("Circle is  not supported for parent");
        }

        public IList<Coordinate> GetAllCoordinates()
        {
            /*https://math.stackexchange.com/questions/198764/how-to-know-if-a-point-is-inside-a-circle	*/
            var startPoint = new Coordinate(this.MinX, this.MinY);
            var allPossiblePoints = GetMaxPossiblePointsOfCircle(startPoint);
            var pointsIsideCircle = FilterCirclePoints(allPossiblePoints);

            return pointsIsideCircle.ToList();
        }


        public double GetArea()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }

        public IList<Coordinate> GetBorderCoordinates()
        {
            var allCoordinates = this.GetAllCoordinates();
            var borderCoordinates = new List<Coordinate>();
            var grouped = allCoordinates.GroupBy(p => p.Y, (Y, points) => new { Y = Y, Points = points.OrderBy(p => p.X) });
            foreach (var item in grouped)
            {
                if (item.Points.Count()>1)
                {
                    borderCoordinates.Add(item.Points.First());
                    borderCoordinates.Add(item.Points.Last());
                }
                else
                {
                    borderCoordinates.Add(item.Points.First());
                }
            }
            return borderCoordinates;
        }

        public void Print()
        {
            var borderPoints = this.GetBorderCoordinates();
            var grouped = borderPoints.GroupBy(b => b.Y);
            foreach (var points in grouped)
            {
                Console.WriteLine(string.Join(",",points));
            }
        }
        #endregion

        #region Helpers
        private IEnumerable<Coordinate> FilterCirclePoints(IEnumerable<Coordinate> allPossiblePoints)
        {

            foreach (var point in allPossiblePoints)
            {
                if (PointIsOnOrInsideCircle(point))
                {
                    yield return point;
                }
            }
        }

        private bool PointIsOnOrInsideCircle(Coordinate point)
        {
            var xSquare = Math.Pow(point.X - this.Center.X, 2);
            var ySquare = Math.Pow(point.Y - this.Center.Y, 2);

            var dSquareRoot = Math.Sqrt(xSquare + ySquare);

            if (dSquareRoot <= this.Radius) return true;
            return false;

        }

        private IEnumerable<Coordinate> GetMaxPossiblePointsOfCircle(Coordinate point)
        {
            point.X = this.MinX;
            while (point.Y <= this.MaxY)
            {
                yield return point;
                point.X++;
                if (point.X >= this.MaxX)
                {
                    point.Y++;
                    point.X = this.MinX;
                }
            }
        }
        #endregion
    }
}
