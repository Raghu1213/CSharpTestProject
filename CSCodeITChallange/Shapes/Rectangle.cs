﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace CSCodeITChallange.Shapes
{
    public class GRectangle : IGeomericShape
    {
        private Coordinate _location;
        private int _width;
        private int _height;

        public IList<Coordinate> Edges
        {
            get;
            set;
        }

        public GRectangle(Coordinate location, int width, int height)
        {
            this._location = location;
            this._width = width;
            this._height = height;
            this.Border = this.GetBorderCoordinates();
            SetEdges();

        }

        #region IGeomericShape

        public IList<Coordinate> GetAllCoordinates()
        {
            var allCoordinates = new List<Coordinate>();
            var startPoint = this._location;
            var maxX = startPoint.X + this._width;
            var maxY = startPoint.Y + this._height;

            var point = startPoint;
            while(point.Y < maxY)
            {
                while (point.X < maxX)
                {
                    allCoordinates.Add(point);
                    point.X++;
                }
                point.X = this._location.X;
                point.Y++;
            }
            return allCoordinates;

        }

        public IList<Coordinate> GetBorderCoordinates()
        {
            var points = new List<Coordinate>();
            var startCoordinates = GetStartCoordinates();
            var endCoordinates = GetEndCoordinates();
            points = DeriveAllCoordinates(startCoordinates.ToList(), endCoordinates.ToList());
            return points;
        }

		/// <summary>
		/// Find intersection points of two shpaes
		/// </summary>
		/// <returns>The intersection.</returns>
		/// <param name="childShape">Child shape.</param>
		public IntersectionPoints FindIntersection(IGeomericShape childShape)
		{
			var allChildBorderPoints = childShape.GetBorderCoordinates();
			var allParentBorderPoints = this.GetBorderCoordinates();

			var intersections = new List<Coordinate>();
			var edgesFallingInside = new List<Coordinate>();

			foreach (var childPoint in allChildBorderPoints)
			{
				foreach (var parentPoint in allParentBorderPoints)
				{
					if (childPoint.Equals(parentPoint))
					{
						intersections.Add((childPoint));
						continue;
					}
				}
			}
			foreach (var edge in childShape.Edges)
			{
				if (edge.X >= _location.X && edge.Y >= _location.Y &&
					edge.X <= _location.X + _width && edge.Y <= _location.Y + _height)
				{
					edgesFallingInside.Add(edge);
				}
			}

			return new IntersectionPoints(intersections, edgesFallingInside);
		}

        public ChildShapeProperties ExtractSubObject(IntersectionPoints intersections)
        {

            var pointsInsideParent = intersections.EdgesFallingInside.Union(intersections.AllIntersections);
            var shape = pointsInsideParent.GroupBy(c => c.Y,
                                                   (key, value) => new { Y = key, row = value.OrderBy(v => v.X) })
                                          .OrderBy((arg) => arg.Y);
            var topLeft = shape.First().row.First();
            var bottomRight = shape.Last().row.Last();
            var width = bottomRight.X - topLeft.X;
            var height = bottomRight.Y - topLeft.Y;
            return new ChildShapeProperties(new List<Coordinate>() { topLeft },
                                            new Dictionary<string, int> { { "WIDTH", width }, { "HEIGHT", height } });

        }

        public double GetArea( )
		{
            return this._height * this._width;
		}

		/// <summary>
		/// Draw the shape on console
		/// </summary>
		public void Draw()
		{
			string space = new string(' ', _width);
			string dot = new string('═', _width);
			string leftFiller = new string(' ', _location.X);

			string shape = string.Concat(leftFiller + dot + Environment.NewLine);

			for (int i = 0; i <= _height; i++)
			{
				shape += leftFiller + "║" + new string(' ', _width - 2) + '║' + Environment.NewLine;
			}
			shape += string.Concat(leftFiller + dot + Environment.NewLine);
			Console.Write(shape);
		}

        public void Print()
        {
           
            Console.WriteLine("Shape with param-->" + this);
            PrintPointsToConsole(this.Border);

            Console.WriteLine("Edges-->");
            PrintPointsToConsole(this.Edges);

        }

		#endregion

		public override string ToString()
		{
			return string.Format("({0} W:{1} H:{2})", this._location, _width, _height);
		}

        #region Private properties
        private IList<Coordinate> Border
        {
            get;
            set;
        }
        #endregion


        private void SetEdges()
        {
            this.Edges = new List<Coordinate>();
            this.Edges.Add(_location);//top left
            this.Edges.Add(new Coordinate(_location.X + _width, _location.Y));//top right
            this.Edges.Add(new Coordinate(_location.X, _location.Y + _height)); // bottom left
            this.Edges.Add(new Coordinate(_location.X + _width, _location.Y + _height)); //bottom right
        }

        private List<Coordinate> DeriveAllCoordinates(List<Coordinate> startCoodintes, List<Coordinate> endCoordinates)
        {
            var firstRowStart = startCoodintes.First();
            var firstRowEnd = endCoordinates.First();
            var lastRowStart = startCoodintes.Last();
            var lastRowEnd = endCoordinates.Last();
            var points = new List<Coordinate>();

            CreateLine(firstRowStart, firstRowEnd, ref points);

            for (int i = 1; i < startCoodintes.Count() - 1; i++)
            {
                points.Add(startCoodintes[i]);
                points.Add(endCoordinates[i]);
            }

            CreateLine(lastRowStart, lastRowEnd, ref points);

            return points;
        }

        private static void CreateLine(Coordinate start, Coordinate end, ref List<Coordinate> points)
        {
            var point = start;
            points.Add(point);
            while (point.X != end.X)
            {
                point.IsBorder = true;
                point.X++;
                points.Add(point);
            }
        }

        private IEnumerable<Coordinate> GetStartCoordinates()
        {
            var refrencePoint = _location;
            for (int i = 0; i <= _height; i++)
            {
                yield return new Coordinate() { X = refrencePoint.X, Y = refrencePoint.Y + i, IsBorder = true };
            }
        }

        private IEnumerable<Coordinate> GetEndCoordinates()
        {
            var refrencePoint = new Coordinate() { X = _location.X + _width, Y = _location.Y };
            for (int i = 0; i <= _height; i++)
            {
                yield return new Coordinate() { X = refrencePoint.X, Y = refrencePoint.Y + i, IsBorder = true };
            }
        }

        private void PrintPointsToConsole(IList<Coordinate> points)
        {

            var groupByY = points.GroupBy(p => p.Y, (key, value) => new { Key = key, Row = value.OrderBy(v => v.X) }).OrderBy(g => g.Key);
            foreach (var grp in groupByY)
            {
                Console.Write(string.Join(",", grp.Row));
                Console.WriteLine();
            }

        }

       
    }
}
