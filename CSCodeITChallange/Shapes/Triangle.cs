﻿using System;
using System.Collections.Generic;

namespace CSCodeITChallange.Shapes
{
    public class Triangle:IGeomericShape
    {
        private  Coordinate P1 { get; }
        private Coordinate P2 { get;  }
        private Coordinate P3 { get; }

        public Triangle(Coordinate p1, Coordinate p2, Coordinate p3)
        {
            P3 = p3;
            P2 = p2;
            P1 = p1;
            Edges = new List<Coordinate> { p1, p2, p3 };
        }

        public IList<Coordinate> Edges { get; set; }

        public void Draw()
        {
            throw new NotImplementedException();
        }

        public ChildShapeProperties ExtractSubObject(IntersectionPoints intersections)
        {
            throw new NotImplementedException();
        }

        public IntersectionPoints FindIntersection(IGeomericShape childShape)
        {
            throw new NotImplementedException();
        }

        public IList<Coordinate> GetAllCoordinates()
        {
            throw new NotImplementedException();
        }

        public double GetArea()
        {
            throw new NotImplementedException();
        }

        public IList<Coordinate> GetBorderCoordinates()
        {
            throw new NotImplementedException();
        }

        public void Print()
        {
            throw new NotImplementedException();
        }
    }
}
